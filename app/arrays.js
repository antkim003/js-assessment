if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function() {
  return {
    indexOf : function (arr, item) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === item) {
                return i;
            } 
        }
        return -1;
    },

    sum : function(arr) {
        var sum = 0;
        for (var i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    },

    remove : function(arr, item) {
        var copy_arr = arr;
        for (var i = 0; i < copy_arr.length; i++) {
            if (copy_arr[i] === item) {
                copy_arr.splice(i,1);
                i -= 1;
            }
        } return copy_arr;
        
    },

    removeWithoutCopy : function(arr, item) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === item) {
                arr.splice(i,1);
                i -= 1;
            }
        } return arr;
    },

    append : function(arr, item) {
        var copy_arr = arr;
        copy_arr.push(item);
        return copy_arr;
    },

    truncate : function(arr) {
        var copy_arr = arr;
        copy_arr.pop();
        return copy_arr;
    },

    prepend : function(arr, item) {
        var copy_arr = arr;
        copy_arr.unshift(item);
        return copy_arr;
    },

    curtail : function(arr) {
        var copy_arr = arr;
        copy_arr.shift();
        return copy_arr;
    },

    concat : function(arr1, arr2) {
        var new_arr = arr1;
        for (var i = 0; i < arr2.length; i++) {
            new_arr.push(arr2[i]);
        } return new_arr;
    },

    insert : function(arr, item, index) {
        arr.splice(2,0,item);
        return arr;
    },

    count : function(arr, item) {
        var count = 0;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === item) {
                count ++;
            }
        } return count;
    },

    duplicates : function(arr) {
        var new_arr = [];
        var count = 0;
        for (var i = 0; i < arr.length; i++) {
            for (var j = i+1; j < arr.length; j++) {
                if (arr[i] === arr[j]) {
                    for (var x = 0; x < new_arr.length; x++) {
                        if (arr[i] === new_arr[x]) {
                            count ++;
                        }
                    }
                    if (count === 0) {
                        new_arr.push(arr[i]);
                    } 
                } 
            } 
        } return new_arr;
    },

    square : function(arr) {
        var new_arr = [];
        console.log(arr);
        for (var i = 0; i < arr.length; i++) {
            new_arr.push(Math.pow(arr[i],2));
        } return new_arr
    },

    findAllOccurrences : function(arr, target) {
        var occurences = [];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === target) {
                occurences.push(i);
            } 
        } return occurences;
    }
  };
});
